public class MineSweeper {
	public static boolean DEBUG = false;
	//Method to print grid and border
	public void print(char[][] a) {
		System.out.print("+");
		for (int i = 0; i < a[0].length; ++i) {
			System.out.print("--");
		}
		System.out.print("+" + "\n");
		for (int i = 0; i < a.length; ++i) {
			System.out.print("|");
			for (int j = 0; j < a[0].length; ++j) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println("|");
		}
		System.out.print("+");
		for (int i = 0; i < a[0].length; ++i) {
			System.out.print("--");
		}
		System.out.print("+" + "\n");
	}
	//Method for Initializing Mines
	public void initializeMines(char[][] a, int numOfMines) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				a[i][j] = '_';
			}
		}
		for (int i = 0; i < numOfMines; i++) {
			// call insert random mine's function
			insertMine(a);

			if (DEBUG) {
				System.out.println("For i=" + i);
				print(a);
			}
		}
	}
	//Method for inserting Random Mines
	private void insertMine(char[][] a) {

		int row = (int) (Math.random() * a.length);
		int col = (int) (Math.random() * a[0].length);
		if (a[row][col] == '*')
			insertMine(a);
		else
			a[row][col] = '*';
	}
	//Method for filling hints
	private void fillHints(char[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				if (a[i][j] != '*') {
					// get num of mines around this cell getMinesAround()
					int num = getMinesAround(a, i, j);
					a[i][j] = (char) ('0' + (char) num);
				}
				if (a[i][j] == '0') {
					a[i][j] = ' ';
				}
			}
		}
	}
	//Method for putting numbers arround mines
	private int getMinesAround(char[][] arr, int row, int col) {
		int mineCount = 0;
		if (row - 1 >= 0 && col - 1 >= 0) {
			if (arr[row - 1][col - 1] == '*')
				++mineCount;
			if (arr[row - 1][col] == '*')
				++mineCount;
			if (arr[row][col - 1] == '*')
				++mineCount;
		}
		if (row - 1 >= 0 && col + 1 < arr[0].length) {
			if (arr[row - 1][col + 1] == '*')
				++mineCount;
		}
		if (row + 1 < arr.length && col - 1 >= 0) {
			if (arr[row + 1][col - 1] == '*')
				++mineCount;
		}
		if (row + 1 < arr.length && col + 1 < arr[0].length) {
			if (arr[row][col + 1] == '*')
				++mineCount;
			if (arr[row + 1][col] == '*')
				++mineCount;
			if (arr[row + 1][col + 1] == '*')
				++mineCount;
		}
		return mineCount;
	}
	//Starting point
	void gameOn(char[][] a) {
		this.initializeMines(a, 10);
		this.fillHints(a);
	}

	public static void main(String[] args) {
		int size1 = 15;
		int size2 = 25;
		char[][] a = new char[size1][size2];
		MineSweeper game = new MineSweeper();
		game.gameOn(a);
		game.print(a);

	}

}
